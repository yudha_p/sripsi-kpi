<?php 
	session_start();
  include '../koneksi.php';
	if(!isset($_SESSION['idb2'])){
    header('location:../index.php');
  }
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Profile</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- join id kepala -->
                          

<!-- tampil semua record -->
<?php
	$sql='SELECT * FROM user
		WHERE id_user='.$_SESSION['idb2'];
		$q=mysql_query ($sql);
		$profile= mysql_fetch_row($q);
			
?>

<?php 
  $b = $profile[9];

  $kar_ids= mysql_query ("SELECT 
    o.nama as kepala
    FROM user p
    INNER JOIN user o
    ON p.kepala=o.id_user WHERE p.kepala=$b ");
  
?>

<!-- pilih atasan -->
<?php 

  $sql1='SELECT * FROM user
		WHERE level="kepala"';
	$hasil1=mysql_query($sql1);

?>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
         <div class="form-group" > 
          <div class="col-md-12">
                 <section class="content">
                      <div class="row">
                      <div class="box">
                        <div class="box-body">
                         <!-- MULAIN ISI -->
                        <form action="ganti_pass_kepala.php?ubahpasswordkepala=<?= $profile[0];?>" method="post">
                          <div class="col-lg-12 ">
                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Nama </label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[1];?></p>
						        </div>
					       </div><br/><br/>
                           <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>NIK</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[2];?></p>
						        </div>
					       </div><br/><br/>
					       <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Jabatan</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[3];?></p>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>No Telepon</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[4];?></p>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Alamat</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[5];?></p>
						        </div>
                            </div><br/><br/>
                            <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Kewarganegaraan</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[6];?></p>
						        </div>
						    </div><br/><br/>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Agama</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= ucwords($profile[7]);?></p>
						        </div>
						    </div><br/><br/>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Jenis Kelamin</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= ucwords($profile[8]);?></p>
						           
						        </div>
						    </div><br/><br/>
						   
						
						  
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Username</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <p><?= $profile[10];?></p>
						        </div>
						    </div><br/><br/>
						   
                            <div class="col-lg-12 "  style="margin-left: 17.5%;">
                            <input type="submit" class="btn btn-danger" value="Ubah Password">
                            </div><br/><br/>

						</form>
                        
                     
                        </div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>

<script src="../assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
