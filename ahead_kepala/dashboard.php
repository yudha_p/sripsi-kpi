<?php 
  session_start();
  if(!isset($_SESSION['idb2'])){
    header('location:../index.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!-- js grafik  -->
  <script src="../assets/dist/js/Chart.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php                   
  if(isset($_POST['tahunsession'])){
    $tahun =$_POST['tahun_fil'];
    $_SESSION['tahun_fil_sess']=$tahun;
   }
  ?>
<?php                   
  if(isset($_POST['bar_session'])){
    $bar =$_POST['bar_sess'];
    $_SESSION['chart']=$bar;
    // header ('location:../ahead_kepala/dashboard.php');
   }
  ?>
 <?php                   
  if(isset($_POST['pie_session'])){
    $pie =$_POST['pie_sess'];
    $_SESSION['chart']=$pie;
    // header ('location:../ahead_kepala/dashboard.php');
   }
  ?>
 <?php                   
  if(isset($_POST['line_session'])){
    $line =$_POST['line_sess'];
    $_SESSION['chart']=$line;
    // header ('location:../ahead_kepala/dashboard.php');
   }
  ?>
  <?php                   
  if(isset($_POST['doughnut_session'])){
    $doughnut =$_POST['doughnut_sess'];
    $_SESSION['chart']=$doughnut;
    // header ('location:../ahead_kepala/dashboard.php');
   }
  ?>

<?php 
  include '../koneksi.php';
    $sql2='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
    $listkpi2= mysql_query($sql2);
?>
<?php 
    $sql3='SELECT * FROM user WHERE id_user='.$_GET['dashboard_link'];
    $listkpi4= mysql_query($sql3);
    $hasil= mysql_fetch_row($listkpi4);
?>



<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
      <div style="float: left;">
       <form action="" method="post">
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label>Filter Tahun</label>&nbsp;&nbsp;&nbsp;
          <input type="text" name="tahun_fil"  style="width: 210px; height: 35px;"/>&nbsp;&nbsp;&nbsp;
          <input type="submit" name="tahunsession" class="btn btn-danger" value="FILTER">
        </form></div>
<div style="float: right; margin-right: 10px;">
 <form action="" method="post">
        <input type="hidden" name="bar_sess" value="bar" />
        <input type="hidden" name="pie_sess" value="pie" />
        <input type="hidden" name="line_sess" value="line" />
        <input type="hidden" name="doughnut_sess" value="doughnut" />
       <button type="submit" name="bar_session" class="fa fa-bar-chart text-danger" title="Bar" style="font-size: 30px;"></button>
       <button type="submit" name="pie_session" class="fa fa-pie-chart text-danger" title="Pie" style="font-size: 30px;"></button>
       <button type="submit" name="line_session" class="fa fa-line-chart text-danger" title="Line" style="font-size: 30px;"></button>
       <button type="submit" name="doughnut_session" class="fa fa-eercast text-danger" title="Doughnut" style="font-size: 30px;"></button>
 </form> 
 </div>        
<!-- MULAI ISI BODY -->
<!-- TAMPILAN GRAFIK  -->
        <div style="width: 1000px;margin: 0px auto;">
          <canvas id="myChart"></canvas>
        </div><br/><br/>
        <!-- ./col -->
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Monitoring <br/>Bulan</th>
                              <th>Nama</th>
                              <th>Nik </th>
                              <th>Jabatan </th>
                              <th>Tahun </th>
                              <th>Nilai Tahun </th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php while($data = mysql_fetch_array($listkpi2)) { ?>
                            <tr>
                              <td><?php echo ucwords($data['bulan_mk']) ?></td>
                              <td><?php echo $data['nama_pk'] ?></td>
                              <td><?php echo $data['nik_pk'] ?></td>
                              <td><?php echo $data['jabatan_pk'] ?></td>
                              <td><?php echo $data['tahun_pk'] ?></td>
                              <td><?php echo $data['nilai_tahun_mk'] ?></td>
                              
                             
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
              
<!-- bar line pie doughnut  -->
      <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
          type: '<?php echo $_SESSION['chart'];  ?>',
          data: {
            labels: ["januari ", "februari", "maret", "april", "mei", "juni", "juli", "agustus", "september", "oktober", "november", "desember"],
            datasets: [{
              label: '<?php echo $_SESSION['tahun_fil_sess'];   ?> <?php echo $hasil[1];?>',
              data: [
                <?php 
               $sql='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="januari" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi= mysql_query($sql);
                $januari =mysql_fetch_array($listkpi);
                // print_r($xxx[16]);
                echo $januari[14];
              ?>, 
              <?php 
               $sql2='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="februari" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi2= mysql_query($sql2);
                $februari =mysql_fetch_array($listkpi2);
                // print_r($xxx[16]);
                echo $februari[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="maret" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="april" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="mei" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="juni" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="juli" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="agustus" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="september" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="oktober" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="november" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>, 
              <?php 
               $sql3='SELECT * FROM perencanaan WHERE tahun_pk="'.$_SESSION['tahun_fil_sess'].'" AND bulan_mk="desember" AND kepala_pk="'.$_SESSION['idb2'].'" AND id_user_karyawan="'.$_GET['dashboard_link'].'"';
               $listkpi3= mysql_query($sql3);
               $desember =mysql_fetch_array($listkpi3);
                // print_r($xxx[16]);
                echo $desember[14];
              ?>
             
              ],
              backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)'
              ],
              borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true
                }
              }]
            }
          }
        });
</script>

     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>
</body>
</html>
