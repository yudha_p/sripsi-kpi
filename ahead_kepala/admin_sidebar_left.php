 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigasi Utama</li>
        <li>
          <a href="../ahead_kepala/dashboard_bawahan.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
          </a>
        </li>
          <li>
          <a href="../ahead_kepala/profile_kepala.php">
            <i class="glyphicon glyphicon-user"></i> <span>Profile</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_kepala/daftar_bawahan.php">
            <i class="glyphicon glyphicon-th-list"></i> <span>Daftar Staff</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_kepala/approved_kpi.php">
            <i class="glyphicon glyphicon-ok-sign"></i> <span>Approved Perencanaan KPI</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_kepala/approved_monitoring_kpi.php">
            <i class="glyphicon glyphicon-ok-sign"></i> <span>Approved Monitoring KPI</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_kepala/monitoring_perencanaan_kpi_bawahan.php">
            <i class="glyphicon glyphicon-tasks"></i> <span>Perencanaan KPI dan <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Monitoring KPI Staff</span> 
          </a>
        </li>
       
        <li>
          <a href="../ahead_kepala/evaluasi_staff.php">
            <i class="glyphicon glyphicon-open"></i> <span>Evaluasi Staff</span> 
          </a>
        </li>
         
     
     
       
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>