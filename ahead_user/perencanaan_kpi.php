
<?php 
  session_start();
  if(!isset($_SESSION['idb3'])){
    header('location:../index.php');
  }
	include '../koneksi.php';
	$sql='SELECT * FROM user
		WHERE id_user='.$_SESSION['idb3'];
		$q=mysql_query($sql);
		$datakaryawan= mysql_fetch_row($q);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Perencanaan KPI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perencanaan KPI
      </h1>
      <ol class="breadcrumb">
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
			  <div class="form-group" > 
          <div class="col-md-12">
                 <section class="content">
                      <div class="row">
                      <div class="box">
                        <div class="box-body">
                         <!-- MULAIN ISI -->
                        <form action="" method="post">

                          <div class="col-lg-12 ">
                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Nama</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="nama_pk" value="<?php echo $datakaryawan[1]; ?>" style="width: 70%;" readonly/>
						        </div>
					       </div><br/><br/>
                           <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>NIK</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="nik_pk" value="<?php echo $datakaryawan[2]; ?>" style="width: 40%;" readonly/>
						        </div>
					       </div><br/><br/>
					       <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Jabatan</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="jabatan_pk" value="<?php echo $datakaryawan[3]; ?>" style="width: 70%;" readonly/>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Tahun</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						           <select name='tahun_pk'>
			                         <option selected disabled>Pilih Tahun</option>
			                         <?php	$tahunbesok = date('Y-m-d'); 
			                                    $kalimat = $tahunbesok;
			                                    $sub_kalimat = substr($kalimat,-10,4);

										                	foreach(range($sub_kalimat, (int)$sub_kalimat+1) as $year) {
												              echo "\t<option  value='".$year."'>".$year."</option>\n\r";
											                }
                                   ?>
			                     </select>
						        </div>
					        </div><br/><br/>
                            
					        <input type="hidden" name="kepala_pk" value="<?php echo $datakaryawan[9]; ?>" />
                  <input type="hidden" name="perencanaan_flag" value="1" />
					        <input type="hidden" name="id_user_karyawan" value="<?php echo $datakaryawan[0]; ?>" />
					        <input type="hidden" name="status_pk" value="draft" />
                            <div class="col-lg-12 "  style="margin-left: 17.5%;">
                            	<input type="submit" name="lanjut_kpi" class="btn btn-danger" value="Lanjut KPI">
                            </div><br/><br/>

						</form>

						 <?php
                   	if(isset($_POST['lanjut_kpi'])){
                       $jangansama='SELECT * FROM perencanaan where id_user_karyawan="'.$_POST["id_user_karyawan"].'" AND tahun_pk="'.$_POST["tahun_pk"].'"';
                       $sama111=mysql_query($jangansama);
                       $sss = mysql_fetch_array($sama111);
                      $nilai =1;
                         if(empty($_POST["tahun_pk"])){
                             $nilai =0;
                             echo "<script type='text/javascript'>";
                             echo "alert('Tahun Harus di isi');";
                             echo "window.location.href='../ahead_user/perencanaan_kpi.php';";
                             echo "</script>";
                          } 
                          if($sss){
                             $nilai =0;
                             echo "<script type='text/javascript'>";
                             echo "alert('Anda Sudah Buat Perencanaan KPI untuk Tahun yang sama');";
                             echo "window.location.href='../ahead_user/perencanaan_kpi.php';";
                             echo "</script>";
                          } 
                          if ($nilai == 1){
                            
                          $sql5='UPDATE user SET status_kpi="'."sudah".'" WHERE id_user='.$_POST['id_user_karyawan'];
                          mysql_query($sql5);

                        	$perencanaan = 'INSERT INTO perencanaan
																(id_user_karyawan, nama_pk, nik_pk, jabatan_pk, tahun_pk, status_pk, kepala_pk, perencanaan_flag)
								                         VALUES
													     ("'.$_POST['id_user_karyawan'].'","'.$_POST['nama_pk'].'","'.$_POST['nik_pk'].'","'.$_POST['jabatan_pk'].'", "'.$_POST['tahun_pk'].'","'.$_POST['status_pk'].'","'.$_POST['kepala_pk'].'","'.$_POST['perencanaan_flag'].'")';
												  $darma = mysql_query($perencanaan);

												}
										if ($darma) { 
                          
										
    									    $id_perencanaan= mysql_insert_id();
    									  	$_SESSION['id_perencanaan'] = $id_perencanaan;
    										      echo "<script type='text/javascript'>";
    								          echo "alert('Lanjut ke KPI');";
    									       	echo "window.location.href='perencanaan_kpi_line.php';";
    								          echo "</script>";
								        }
										
								}
								?>
                       </div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>
</body>
</html>
