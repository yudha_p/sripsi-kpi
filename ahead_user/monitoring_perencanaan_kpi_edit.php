<?php 
  session_start();
  if(!isset($_SESSION['idb3'])){
    header('location:../index.php');
  }
	include '../koneksi.php';
	$sql='SELECT * FROM perencanaan
		WHERE id_perencanaan='.$_GET['editperencanaan'];
		$q=mysql_query($sql);
		$perencanaankpi= mysql_fetch_row($q);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Ubah Perencanaan KPI </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- table -->
  <link rel="stylesheet" href="../assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php 
	
  $perencanaankpi99 = $_GET['editperencanaan'];
  $line_listkpi= mysql_query ("SELECT 
    p.id_line_perencanaan as id_line_perencanaan,
    o.master_key_performance_indicator as key_performance_indicator,
    o.master_perspektif as perspektif, 

    p.bobot_line_kpi as bobot_line_kpi, 
    p.rencana_awal_tahun as rencana_awal_tahun,
    p.rencana_bulan_ini as rencana_bulan_ini,
    p.perencanaan_kpi as 	perencanaan_kpi
    FROM line_perencanaan p
    INNER JOIN master_kpi o
    ON p.key_performance_indicator=o.id_master_kpi WHERE p.perencanaan_kpi=$perencanaankpi99 ");

?>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Ubah Perencanaan KPI
      </h1>
      <ol class="breadcrumb">
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
			  <div class="form-group" > 
                <div class="col-md-12">
               <form action="../ahead_user/proses_edit_all_user.php" method="post">
                <input type="submit" name="simpaneditkpi" class="btn btn-danger" value="Simpan"/>
                 <input type="hidden" name="id_perencanaan" value="<?php echo $_GET['editperencanaan'] ?>" />
                 <section class="content">
                      <div class="row">
                      <div class="box">
                        <div class="box-body">
                         <!-- MULAIN ISI -->

                          <div class="col-lg-12 ">
                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Nama</label>
						        </div>
						        <div class="col-lg-6 ">
						          <!--data input -->
						          <input type="text" name="nama_pk" value="<?php echo $perencanaankpi[2]; ?>" style="width: 70%;" readonly/>
						        </div>
					       
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Tahun</label>
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
						           <select name='tahun_pk'>
						              
			                            <?php
												$tahunbesok = date('Y-m-d'); 
			                                    $kalimat = $tahunbesok;
			                                    $sub_kalimat = substr($kalimat,-10,4);

											foreach(range($sub_kalimat, (int)$sub_kalimat+1) as $year) {
												if ($perencanaankpi[6] == $year){
											    	echo "\t<option  selected='selected' value='".$year."'>".$year."</option>\n\r";
												}else{
											    	echo "\t<option  value='".$year."'>".$year."</option>\n\r";
												}
												
											}


											?>
			                     </select>
						        </div>
					       </div><br/><br/>
					       <!-- baris kedua -->
					       <div class="col-lg-12 ">
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>NIK</label>
						        </div>
						        <div class="col-lg-6">
						          <!--data input -->
						          <input type="text" name="nik_pk" value="<?php echo $perencanaankpi[3]; ?>" style="width: 70%;" readonly/>
						        </div>
					       
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
						        
						        </div>
					        </div><br/><br/>
					         <div class="col-lg-12 ">
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Jabatan</label>
						        </div>
						        <div class="col-lg-6">
						          <!--data input -->
						          <input type="text" name="nik_pk" value="<?php echo $perencanaankpi[4]; ?>" style="width: 70%;" readonly/>
						        </div>
						        <div class="col-lg-1 ">
						          <!-- data label -->
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
						        </div>
					        </div><br/><br/><br/>
                            <!-- TOMBOL TAMBAH  -->
                <div class="col-lg-12 ">
                	<div class="col-lg-12 ">
             						<a href="../ahead_user/monitoring_perencanaan_kpi_tambah_edit_line.php?tambaheditkpi=<?php echo $perencanaankpi[0]; ?>" class="btn btn-danger" >Tambah KPI</a>
         						</div>
     						</div><br/><br/>
					        <!-- MULAI DI TABLE  -->
					        <div class="col-lg-12 ">
					        <!-- UNTUK TABLE LINE PERENCANAAN  -->
					        <div class="col-lg-12 ">
		                        <table id="example1" class="table table-bordered table-striped">
		                          <thead>
		                            <tr>
		                              <th>Key Performance Indicator</th>
		                              <th>Perspektif </th>
		                              <th>Bobot </th>
		                              <th>Rencana Awal Tahun </th>
		                              <th>Rencana Bulan Ini</th>
		                              <th>Aksi</th>
		                           </tr>
		                          </thead>
		                          <tbody>
		                          <?php while($data = mysql_fetch_array($line_listkpi)) { ?>
		                            <tr>
		                              <td><?php echo $data['key_performance_indicator'] ?></td>
		                              <td><?php echo $data['perspektif'] ?></td>
		                              <td><p align="right"><?php echo $data['bobot_line_kpi'] ?></p></td>
		                              <td><p align="right"><?php echo number_format($data['rencana_awal_tahun']) ?></p></td>
		                              <td><p align="right"><?php echo number_format($data['rencana_bulan_ini']) ?></p></td>
		                              <td>
		                              	<a href="../ahead_user/monitoring_perencanaan_kpi_edit_line.php?editperencanaanline=<?php echo $data['id_line_perencanaan'] ?>" class="btn-danger btn-sm">Ubah</a>
                              <a href="../ahead_user/delete_all_user.php?deleteperencanaanline=<?php echo $data['id_line_perencanaan'] ?>" class="btn-danger  btn-sm" onclick="return confirm('Apakah ingin menghapus data ini?');">Hapus</a>
		                              </td>
		                             
		                            </tr>
		                            <?php  } ?>
		                          </tbody>
		                        </table>
		                        </div>
                            </div><br/><br/>
					     </form>
						</div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>

<script src="../assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
