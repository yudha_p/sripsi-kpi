 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigasi Utama</li>
        <li>
          <a href="../ahead_user/beranda.php">
            <i class="glyphicon glyphicon-home"></i><span>Beranda</span> 
          </a>
        </li>
      
        <li>
          <a href="../ahead_user/profile_user.php">
            <i class="glyphicon glyphicon-user"></i> <span>Profile</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_user/perencanaan_kpi.php">
            <i class="glyphicon glyphicon-list-alt"></i> <span>perencanaan KPI</span> 
          </a>
        </li>
         <li>
          <a href="../ahead_user/list_perencanaan_kpi.php">
            <i class="glyphicon glyphicon-list-alt"></i> <span>Daftar perencanaan KPI</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_user/monitoring_perencanaan_kpi_revisi.php">
            <i class="fa fa-edit"></i> <span>Revisi</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_user/monitoring_kpi_perbulan.php">
            <i class="glyphicon glyphicon-eye-open"></i> <span>Monitoring KPI</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_user/monitoring_perencanaan_kpi.php">
            <i class="glyphicon glyphicon-tasks"></i> <span>Daftar Monitoring KPI</span> 
          </a>
        </li>
        <li>
          <a href="../ahead_user/evaluasi.php">
            <i class="glyphicon glyphicon-open"></i> <span>Evaluasi</span> 
          </a>
        </li>
       
     
       
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>