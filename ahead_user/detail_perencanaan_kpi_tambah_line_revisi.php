<?php 	
  session_start();
  if(!isset($_SESSION['idb3'])){
    header('location:../index.php');
  }
	include '../koneksi.php';
    $sql='SELECT * FROM master_kpi';
	$master_kpi=mysql_query($sql);

	 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Key Performance Indicator</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Key Performance Indicator
      </h1>
      <ol class="breadcrumb">
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
			  <div class="form-group" > 
          <div class="col-md-12">
                 <section class="content">
                      <div class="row">
                      <div class="box">
                        <div class="box-body">
                         <!-- MULAIN ISI -->
                        <form action="" method="post">

                          <div class="col-lg-12 ">
                            <div class="col-lg-3 ">
						          <!-- data label -->
						          <label>Key Performance Indicator</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <select name="key_performance_indicator" id="luas_area" required="required" class="form-control" style="width: 34%;" >
											<option selected="selected" value="none" disabled>-- Pilih KPI --</option>
                                        <?php while($data = mysql_fetch_array($master_kpi)) { ?>
											<option value="<?php echo $data['id_master_kpi'] ?>"><?php echo $data['master_key_performance_indicator'] ?></option>
										<?php  } ?>	
								    </select>
						        </div>
					       </div><br/><br/>
                        
					       <div class="col-lg-12 ">
	                            <div class="col-lg-3 ">
						          <!-- data label -->
						          <label>Bobot</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="bobot_line_kpi" style="width: 70%;" />
						        </div>
					        </div><br/><br/>
					       <div class="col-lg-12 ">
	                            <div class="col-lg-3 ">
						          <!-- data label -->
						          <label>Rencana Awal Tahun</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="rencana_awal_tahun" style="width: 70%;" />
						        </div>
					        </div><br/><br/>
					       <div class="col-lg-12 ">
	                            <div class="col-lg-3 ">
						          <!-- data label -->
						          <label>Rencana Bulan ini</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="rencana_bulan_ini" style="width: 70%;" />
						        </div>
					        </div><br/><br/>
					        
					        <input type="hidden" name="perencanaan_kpi" value="<?php echo $_GET['tambahlinekpi'] ?>" />
                            <div class="col-lg-12 "  style="margin-left: 25.5%;">
                            	<input type="submit" name="done" class="btn btn-danger" value="Simpan">
                            </div><br/><br/>

						</form>

					
						 <?php
                                 	if(isset($_POST['done'])){

                                        $id_awal = $_POST['perencanaan_kpi'];  
													$perencanaan_line2 = 'INSERT INTO line_perencanaan
																(key_performance_indicator, perspektif, bobot_line_kpi, rencana_awal_tahun, rencana_bulan_ini, perencanaan_kpi)
								                         VALUES
													     ("'.$_POST['key_performance_indicator'].'","'.$_POST['key_performance_indicator'].'","'.$_POST['bobot_line_kpi'].'", "'.$_POST['rencana_awal_tahun'].'","'.$_POST['rencana_bulan_ini'].'","'.$_POST['perencanaan_kpi'].'")';
												$darma_line2 = mysql_query($perencanaan_line2);
												
										if ($darma_line2) { 
										echo "<script type='text/javascript'>";
								        echo "alert('Simpan KPI');";
									 	echo "window.location.href='../ahead_user/detail_perencanaan_kpi_revisi.php?revisiperencanaan=$id_awal';";
								        echo "</script>";
								        }
										
								}
								?>
                       </div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>
</body>
</html>