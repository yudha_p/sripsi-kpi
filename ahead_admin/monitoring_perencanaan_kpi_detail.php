
<?php 
  session_start();
	include '../koneksi.php';
  if(!isset($_SESSION['idb1'])){
    header('location:../index.php');
  }
	$sql='SELECT * FROM perencanaan
		WHERE id_perencanaan='.$_GET['detailmonitoringadmin'];
		$q=mysql_query($sql);
		$approvedmonitoring= mysql_fetch_row($q);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Monitoring Perencanaan KPI </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- table -->
  <link rel="stylesheet" href="../assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php 
	
  $approvedkpi99 = $_GET['detailmonitoringadmin'];
  $line_listkpi= mysql_query ("SELECT 
    p.id_line_perencanaan as id_line_perencanaan,
    o.master_key_performance_indicator as key_performance_indicator,
    o.master_perspektif as perspektif, 

    p.bobot_line_kpi as bobot_line_kpi, 
    p.rencana_awal_tahun as rencana_awal_tahun,
    p.rencana_bulan_ini as rencana_bulan_ini,
    p.perencanaan_kpi as 	perencanaan_kpi,
    p.realisasi_bulan_ini as  realisasi_bulan_ini,
    p.persen_bulan_ini as  persen_bulan_ini,
    p.nilai_bulan_ini as  nilai_bulan_ini,
    p.total_realisasi_all as  total_realisasi_all,
    p.persen_tahun as    persen_tahun,
    p.grand_nilai_tahun as  grand_nilai_tahun,
    p.rencana_bulan_depan as  rencana_bulan_depan
    FROM line_perencanaan p
    INNER JOIN master_kpi o
    ON p.key_performance_indicator=o.id_master_kpi WHERE p.perencanaan_kpi=$approvedkpi99 ");

?>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Monitoring Perencanaan KPI
      </h1>
      <ol class="breadcrumb">
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
			  <div class="form-group" > 
              <div class="col-md-12">
               <section class="content">
                      <div class="row">
                      <div class="box">

                        <div class="box-body">
                         <!-- MULAIN ISI -->
                        <form action="" method="post">

                          <div class="col-lg-12 ">
                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Nama</label>
						        </div>
						        <div class="col-lg-6 ">
						          <!--data input -->
						          <input type="text"  value="<?php echo $approvedmonitoring[2]; ?>" style="width: 70%;" readonly/>
						        </div>
					       
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Tahun</label>
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
						         <input type="text"  value="<?php echo $approvedmonitoring[6]; ?>" style="width: 70%;" readonly/>
						        </div>
					       </div><br/><br/>
					       <!-- baris kedua -->
					       <div class="col-lg-12 ">
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>NIK</label>
						        </div>
						        <div class="col-lg-6">
						          <!--data input -->
						          <input type="text" value="<?php echo $approvedmonitoring[3]; ?>" style="width: 70%;" readonly/>
						        </div>
					       
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Bulan</label>
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
						          <input type="text" value="<?php echo ucwords($approvedmonitoring[10]); ?>" style="width: 70%;" readonly/>
						        </div>
					        </div><br/><br/>
					         <div class="col-lg-12 ">
	                            <div class="col-lg-1 ">
						          <!-- data label -->
						          <label>Jabatan</label>
						        </div>
						        <div class="col-lg-6">
						          <!--data input -->
						          <input type="text"  value="<?php echo $approvedmonitoring[4]; ?>" style="width: 70%;" readonly/>
						        </div>
						        <div class="col-lg-1 ">
						          <!-- data label -->
                      <label>Status Monitoring</label>
						        </div>
						        <div class="col-lg-4">
						          <!--data input -->
                     <input type="text" value="<?php echo ucwords($approvedmonitoring[9]); ?>" style="width: 70%;" readonly/>
						        </div>
					        </div><br/><br/><br/><br/>
					        <!-- MULAI DI TABLE  -->
					        <div class="col-sx-12 ">
					        <!-- UNTUK TABLE LINE PERENCANAAN  -->
		                        <table id="example1" class="table table-bordered table-striped">
		                          <thead>
		                            <tr>
		                              <th>Key Performance Indicator</th>
		                              <th>Perspektif </th>
		                              <th>Bobot </th>
		                              <th>Rencana Awal Tahun </th>
		                              <th>Rencana Bulan Ini</th>
                                  <th>Realisasi Bulan</th>
                                  <th>% Bulan</th>
                                  <th>Nilai Bulan</th>
                                  <th>Total Realisasi</th>
                                  <th>% Tahun</th>
                                  <th>Nilai Tahun</th>
                                  <th>Rencana Bulan Depan</th>
		                           </tr>
		                          </thead>
		                          <tbody>
		                          <?php while($data = mysql_fetch_array($line_listkpi)) { ?>
		                            <tr>
		                              <td><?php echo $data['key_performance_indicator'] ?></td>
		                              <td><?php echo $data['perspektif'] ?></td>
		                              <td><p align="right"><?php echo $data['bobot_line_kpi'] ?></p></td>
		                              <td><p align="right"><?php echo number_format($data['rencana_awal_tahun']) ?></p></td>
		                              <td><p align="right"><?php echo number_format($data['rencana_bulan_ini']) ?></p></td>
                                  <td><p align="right"><?php echo number_format($data['realisasi_bulan_ini']) ?></p></td>
                                  <td><p align="right"><?php echo $data['persen_bulan_ini'] ?></p></td>
                                  <td><p align="right"><?php echo $data['nilai_bulan_ini'] ?></p></td>
                                  <td><p align="right"><?php echo number_format($data['total_realisasi_all']) ?></p></td>
                                  <td><p align="right"><?php echo $data['persen_tahun'] ?></p></td>
                                  <td><p align="right"><?php echo $data['grand_nilai_tahun'] ?></p></td>
                                  <td><p align="right"><?php echo number_format($data['rencana_bulan_depan']) ?></p></td>
		                             
		                            </tr>
		                            <?php  } ?>
		                          </tbody>
		                        </table>
                            </div><br/><br/>
          					     </form>
          						</div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>

<script src="../assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
