

<?php 
	session_start();
    include '../koneksi.php';
    if(!isset($_SESSION['idb1'])){
    header('location:../index.php');
  }

	
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Edit Karyawan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- join id kepala -->
                          

<!-- tampil semua record -->
<?php
	$sql='SELECT * FROM user
		WHERE id_user='.$_GET['karyawan'];
		$q=mysql_query ($sql);
		$hasil= mysql_fetch_row($q);
			
?>

<?php 
  $b = $hasil[9];

  $kar_ids= mysql_query ("SELECT 
    o.nama as kepala
    FROM user p
    LEFT OUTER JOIN user o
    ON p.kepala=o.id_user WHERE p.kepala=$b ");
  
?>

<!-- pilih atasan -->
<?php 

  $sql1='SELECT * FROM user
		WHERE level="kepala"';
	$hasil1=mysql_query($sql1);

?>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Karyawan
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
         <div class="form-group" > 
          <div class="col-md-12">
                 <section class="content">
                      <div class="row">
                      <div class="box">
                        <div class="box-body">
                         <!-- MULAIN ISI -->
                        <form action="proses_edit_all.php" method="post">
                          <div class="col-lg-12 ">
                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Nama </label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="nama" style="width: 70%;" value="<?= $hasil[1];?>" />
						        </div>
					       </div><br/><br/>
                           <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>NIK</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="nik" style="width: 40%;" value="<?= $hasil[2];?>"/>
						        </div>
					       </div><br/><br/>
					       <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Jabatan</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="jabatan" style="width: 70%;" value="<?= $hasil[3];?>"/>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Email</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="email" name="email" style="width: 70%;" value="<?= $hasil[13];?>"/>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>No Telepon</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="no_tlp" maxlength="14" style="width: 30%;" value="<?= $hasil[4];?>"/>
						        </div>
					        </div><br/><br/>
					        <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Alamat</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="textarea" name="alamat"  style="width: 100%; " value="<?= $hasil[5];?>"/>
						        </div>
                            </div><br/><br/>
                            <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Kewarganegaraan</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="kewarganegaraan" style="width: 50%;" value="<?= $hasil[6];?>"/>
						        </div>
						    </div><br/><br/>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Agama</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						            <select name="agama" id="luas_area" required="required" class="form-control" style="width: 34%;" >
						                    <option selected="selected" value="<?= $hasil[7];?>" ><?= ucwords($hasil[7]);?></option>
											
											<option value="Islam">Islam</option>
											<option value="Kristen">Kristen</option>
											<option value="Katolik">Katolik</option>
											<option value="Buddha">Buddha</option>
											<option value="Kong Hu Cu">Kong Hu Cu</option>
											<option value="Agama Hindu">Agama Hindu</option>
								    </select>
						        </div>
						    </div><br/><br/>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Jenis Kelamin</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						            <select name="jenis_kelamin" id="luas_area" required="required" class="form-control" style="width: 44%;" >
											<option selected="selected"value="<?= $hasil[8];?>" ><?= ucwords($hasil[8]);?></option>
											<option value="pria">Pria</option>
											<option value="wanita">Wanita</option>
								    </select>
						        </div>
						    </div><br/><br/>
						    <?php 
						              $r = $_GET['karyawan'];
									  $sql2='SELECT * FROM user
											WHERE id_user='.$r.' and level="kepala"   ';
											$hasil2=mysql_query($sql2);
						          if(mysql_fetch_array($hasil2)){
						          } else{ ?>  
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Kepala</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						            
						            <select name="kepala" id="luas_area" required="required" class="form-control" style="width: 34%;" >
											<option selected="selected"value="<?= $hasil[9];?>" >
											<?php $data = mysql_fetch_array($kar_ids); echo $data['kepala'];  ?></option>
                                        <?php while($data1 = mysql_fetch_array($hasil1)) { ?>
											<option value="<?php echo $data1['id_user'] ?>"><?php echo $data1['nama'] ?></option>
										<?php  } ?>	
								    </select>
								    
						        </div>
						    </div><br/><br/>
						    <?php } ?>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Username</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="text" name="user_name" style="width: 40%;" value="<?= $hasil[10];?>"/>
						        </div>
						    </div><br/><br/>
						    <div class="col-lg-12 ">
	                            <div class="col-lg-2 ">
						          <!-- data label -->
						          <label>Password</label>
						        </div>
						        <div class="col-lg-6 col-xs-8">
						          <!--data input -->
						          <input type="password" name="password" style="width: 40%;" value=""/>
						        </div>
						    </div><br/><br/>
						    
                            <div class="col-lg-12 "  style="margin-left: 17.5%;">
                            <input type ="hidden" name ="id_user" value="<?= $_GET['karyawan'];?>"/>
                            <input type="submit" name="ubah" class="btn btn-danger" value="Ubah">
                            </div><br/><br/>

						</form>
                        
                     
                        </div>
                      </div>
                  </div>
                </section>
               </div>
            </div>


        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>

<script src="../assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
