<?php 
	session_start();
	if(!isset($_SESSION['idb1'])){
    header('location:../index.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../gambar/logo_adhimix_mini.png" type="image/png" sizes="24x24">
  <title>Perencanaan dan Monitoring KPI</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/admin/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/admin/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../assets/admin/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../assets/admin/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../assets/admin/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../assets/admin/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <link rel="stylesheet" href="../assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css">



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<?php 
  include '../koneksi.php';

 
  
  //$sus= mysql_query ("SELECT * FROM pembayaran ");
  $kar_ids= mysql_query ("SELECT 
    p.id_user as id_user,
    p.nama as nama,
    p.nik as nik, 
    p.jabatan as jabatan,
    p.jenis_kelamin as jenis_kelamin,
    o.nama as kepala,
    p.tahun_kpi as tahun_kpi,
    p.status_kpi as status_kpi,
    p.status_monitoring_bulan_ini as status_monitoring_bulan_ini
    FROM user p
    LEFT OUTER JOIN user o
    ON p.kepala=o.id_user WHERE p.level='user' ");

?>
<body class="hold-transition skin-red sidebar-mini">


<div class="wrapper">

  <?php include "admin_head.php" ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "admin_sidebar_left.php" ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perencanaan dan Monitoring KPI
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
<!-- MULAI ISI BODY -->
         <div class="form-group" > 
          <div class="col-md-12">
           <a href="../ahead_admin/update_status_kpi_karyawan.php" class="btn btn-danger" onclick="return confirm('Apakah anda yakin mengubah status KPI untuk Awal Tahun?')">Status KPI Awal Tahun</a>
           <a href="../ahead_admin/update_status_monitoring_kpi_karyawan.php" class="btn btn-danger" onclick="return confirm('Apakah anda yakin mengubah status Monitoring KPI untuk Bulan ini?')">Status Monitoring Bulan ini</a>
                 <section class="content">
                      <div class="row">
                      <div class="box">
                      
                        <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>Nama</th>
                              <th>Nik </th>
                              <th>Jabatan </th>
                              <th>Kepala</th>
                              <th>Tahun KPI</th>
                              <th>Status KPI</th>
                              <th>Status Monitoring Bulan ini</th>
                              <th>Aksi</th>
                              
                           
                            </tr>
                          </thead>
                          <tbody>
                          <?php while($data = mysql_fetch_array($kar_ids)) { ?>
                            <tr>
                              <td><?php echo $data['nama'] ?></td>
                              <td><?php echo $data['nik'] ?></td>
                              <td><?php echo $data['jabatan'] ?></td>
                              <td><?php echo $data['kepala'] ?></td>
                              <td><?php echo $data['tahun_kpi'] ?></td>
                              <td><?php echo ucwords($data['status_kpi']) ?></td>
                              <td><?php echo ucwords($data['status_monitoring_bulan_ini']) ?></td>
                              <td>
                              <a href="../ahead_admin/monitoring_perencanaan_kpi.php?view_monitoring=<?php echo $data['id_user'] ?>" class="btn-danger  btn-sm">lihat</a>
                              </td>
                            </tr>
                            <?php  } ?>
                          </tbody>
                        </table>
                        </div>
                      </div>
                  </div>
                </section>
               </div>
            </div>

        <!-- ./col -->
     
        <!-- ./col -->
        
        <!-- ./col -->
      </div>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "admin_footer.php" ?>

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
</div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/admin/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../assets/admin/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../assets/admin/raphael/raphael.min.js"></script>
<script src="../assets/admin/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../assets/admin/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../assets/admin/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../assets/admin/moment/min/moment.min.js"></script>
<script src="../assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../assets/admin/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../assets/admin/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../assets/admin/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../assets/dist/js/demo.js"></script>

<script src="../assets/admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
