 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Navigasi Utama</li>
        <li>
          <a href="../ahead_admin/all_karyawan.php">
            <i class="glyphicon glyphicon-th-list"></i> <span>Karyawan</span> 
          </a>
        </li>
      
          <li>
          <a href="../ahead_admin/monitoring_perencanaan_kpi_list.php">
            <i class="glyphicon glyphicon-tasks"></i> <span>Semua Perencanaan KPI dan <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Monitoring KPI</span> 
          </a>
        </li>
        <li>
          <a href="evaluasi_all.php">
            <i class="glyphicon glyphicon-tasks"></i> <span>Evaluasi</span> 
          </a>
        </li>
                
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-cog"></i>
            <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../ahead_admin/master_kpi.php"><i class="glyphicon glyphicon-signal"></i>Master KPI</a></li>
            <li><a href="../ahead_admin/setting_admin.php?adminsetting=<?php echo $_SESSION['idb1'] ?>"><i class="glyphicon glyphicon-signal"></i>Setting ADMIN</a></li>
          </ul>
          
        </li>
     
       
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>